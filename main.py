import sys


class Job:
    def __init__(self, runtime, nb, walltime):
        self.runtime = runtime
        self.nb = nb
        self.walltime = walltime

    def to_oarsub(self, speed=1.0):
        nb = self.nb
        wt = int(self.walltime/speed)
        sleep = self.runtime/speed
        return f"oarsub -l host={nb},walltime=0:0:{wt} 'sleep {sleep}'"


def read_swf(filename):
    data = []
    with open(filename, "r") as swf_file:
        for line in swf_file.readlines():
            if len(line) == 0 or line[0] == ';':
                continue

            job = line.split()
            if len(job) == 0:
                continue
            job_subtime = int(job[1])
            job_runtime = int(job[3])
            job_nb = int(job[4])
            job_walltime = int(job[8])

            data.append((job_subtime, Job(job_runtime, job_nb, job_walltime)))
    return data


def generate_bash_script(data, filename, speed=1.0):
    previous_subtime = None
    with open(filename, "w") as bash_file:
        for (subtime, job) in data:
            sleep = (subtime - previous_subtime)/speed if previous_subtime\
                                                       else 0.0
            bash_file.write(f"sleep {sleep}; {job.to_oarsub(speed)}\n")
            previous_subtime = subtime


def main():
    args = sys.argv
    if len(args) != 3:
        print(f"Usage {args[0]} input.swf output.bash")
        return 1

    filename = args[1]
    outname = args[2]

    data = read_swf(filename)
    generate_bash_script(data, outname, 1.0)


if __name__ == "__main__":
    main()
