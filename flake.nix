{
  description = "A very basic flake";

  inputs = { nixpkgs.url = "github:nixos/nixpkgs/21.11"; };

  outputs = { self, nixpkgs }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; };
    in {

      packages.x86_64-linux.swf_to_oar =
        pkgs.writers.writePython3Bin "swf_to_oar" { libraries = [ ]; }
        (builtins.readFile ./main.py);
    };
}
